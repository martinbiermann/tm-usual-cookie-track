application_root = __dirname
express = require "express"
path = require "path"
Cookies = require "cookies"

console.log "Started..."

http = require 'http'
app = express()

app.configure () ->
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use app.router
  app.use(express.static(path.join(application_root, "public")))
  app.use(express.errorHandler({dumpExceptions: true, showStack: true}))

app.get "/track", (req, res) ->
  cookies = new Cookies(req, res);
  cookieId = cookies.get "cookieId"
  if cookieId is null
  	console.log "Missing cookie."
  	console.log "Publish cookie."
  	cookies.set "cookieId", "randomValue"
  else
  	console.log "Found cookie with id "+cookieId

  if !!req.query.tmAppScheme
    out = "<!DOCTYPE html><html>"
    out += "<meta http-equiv=\"refresh\" content=\"0.7;URL=%TMAppScheme%://tmreturn\">"
    out += "<head>"

    out += "<script>"

    out += "window.onload = function() {"

    out += "var script = document.createElement(\"img\");"
    out += "script.src = \"http://www.awf.org/files/3779_file_Elephant2_Balfour.jpg\";"
    out += "script.style.cssText = \"display: none;\";"
    out += "script.onload = function () {"
    out += "window.location = \"tmwebtrack://tmreturn\";"
    out += "};"
    out += "document.body.appendChild(script);"

    out += "};"

    out += "</script>"
    out += "</head>"
    out += "<body><img src=\"http://www.awf.org/files/3779_file_Elephant2_Balfour.jpg\" style=\"display:none;\" /></body></html>"
    out =  out.replace /%TMAppScheme%/, req.query.tmAppScheme 
    res.end out
  else
    res.end()

app.listen 4242
